package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saadahmedev.githubandroidrepo.domain.useCase.GithubRepositoryListUseCase
import com.saadahmedev.githubandroidrepo.util.ResponseState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GithubRepositoryViewModel @Inject constructor(private val githubRepositoryListUseCase: GithubRepositoryListUseCase) : ViewModel() {

    private val _repositoryList = MutableStateFlow(GithubRepositoryListResponseState())
    val repositoryList: StateFlow<GithubRepositoryListResponseState> = _repositoryList

    fun getRepositories(repoType: String, sortBy: String) = viewModelScope.launch(Dispatchers.IO) {
        githubRepositoryListUseCase.invoke(repoType, sortBy).collect {
            when (it) {
                is ResponseState.Loading -> {
                    _repositoryList.value = GithubRepositoryListResponseState(isLoading = true)
                }

                is ResponseState.Success -> {
                    _repositoryList.value = GithubRepositoryListResponseState(repositoryList = it.data?: emptyList())
                }

                is ResponseState.Error -> {
                    _repositoryList.value = GithubRepositoryListResponseState(error = it.message!!)
                }
            }
        }
    }
}