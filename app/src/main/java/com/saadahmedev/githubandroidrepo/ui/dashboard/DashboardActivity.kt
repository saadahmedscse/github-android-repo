package com.saadahmedev.githubandroidrepo.ui.dashboard

import android.os.Bundle
import com.saadahmedev.githubandroidrepo.base.BaseActivity
import com.saadahmedev.githubandroidrepo.databinding.ActivityDashboardBinding
import com.saadahmedev.githubandroidrepo.databinding.AppToolbarBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardActivity : BaseActivity<ActivityDashboardBinding>(ActivityDashboardBinding::inflate) {

    override val toolbarBinding: AppToolbarBinding
        get() = binding.appToolbar

    override fun onActivityCreate(savedInstanceState: Bundle?) {}

    override fun observeData() {}
}