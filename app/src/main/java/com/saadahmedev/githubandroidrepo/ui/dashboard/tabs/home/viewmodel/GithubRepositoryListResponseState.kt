package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.viewmodel

import com.saadahmedev.githubandroidrepo.domain.model.Repository

data class GithubRepositoryListResponseState(
    val isLoading: Boolean = false,
    val repositoryList: List<Repository> = emptyList(),
    val error: String = ""
)