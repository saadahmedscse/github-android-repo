package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.repoDetails

import android.annotation.SuppressLint
import android.os.Bundle
import com.saadahmedev.githubandroidrepo.R
import com.saadahmedev.githubandroidrepo.base.BaseFragment
import com.saadahmedev.githubandroidrepo.databinding.FragmentRepoDetailsBinding
import com.saadahmedev.githubandroidrepo.domain.model.Repository
import com.saadahmedev.githubandroidrepo.helper.gone
import com.saadahmedev.githubandroidrepo.helper.visible
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.Locale

class RepoDetailsFragment : BaseFragment<FragmentRepoDetailsBinding>(FragmentRepoDetailsBinding::inflate) {

    override val title: String
        get() = session.getObject<Repository>(Repository::class.java)?.repoName.toString()
    override val isBackButtonVisible: Boolean
        get() = true

    @SuppressLint("SetTextI18n")
    override fun onFragmentCreate(savedInstanceState: Bundle?) {
        val repository: Repository = session.getObject(Repository::class.java)!!
        binding.item = repository
        Picasso.get().load(repository.avatarUrl).into(binding.ivAvatar)

        if (repository.topics.size >= 3) {
            binding.topicLayout.visible()

            binding.topic1.text = repository.topics[0]
            binding.topic2.text = repository.topics[1]
            binding.topic3.text = repository.topics[2]
        }

        if (repository.language == null) binding.languageLayout.gone()

        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        val newFormat = SimpleDateFormat("MMM-dd-yyyy HH:ss", Locale.getDefault())
        val time = sdf.parse(repository.updatedAt!!)
        val lastUpdateTime = newFormat.format(time!!)
        binding.tvLastUdpateTime.text = "Last Updated: $lastUpdateTime"
    }

    override fun observeData() {}
}