package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.adapter

import com.saadahmedev.githubandroidrepo.domain.model.Repository

interface OnItemClickListener {
    fun onClicked(item: Repository, position: Int)
}