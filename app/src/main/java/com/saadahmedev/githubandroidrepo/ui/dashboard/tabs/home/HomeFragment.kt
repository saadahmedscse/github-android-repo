package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.saadahmedev.githubandroidrepo.R
import com.saadahmedev.githubandroidrepo.base.BaseFragment
import com.saadahmedev.githubandroidrepo.databinding.FragmentHomeBinding
import com.saadahmedev.githubandroidrepo.domain.model.Repository
import com.saadahmedev.githubandroidrepo.helper.setLinearLayoutManager
import com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.adapter.GithubRepositoryAdapter
import com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.adapter.OnItemClickListener
import com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.viewmodel.GithubRepositoryViewModel
import com.saadahmedev.githubandroidrepo.util.AppConstants.AppInfo.APP_NAME
import com.saadahmedev.githubandroidrepo.util.ProgressDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate), OnItemClickListener {

    override val title: String
        get() = APP_NAME
    override val isBackButtonVisible: Boolean
        get() = false

    private val githubRepoViewModel by viewModels<GithubRepositoryViewModel>()
    private lateinit var progressDialog: ProgressDialog

    private val githubRepositoryAdapter by lazy {
        GithubRepositoryAdapter(this)
    }

    override fun onFragmentCreate(savedInstanceState: Bundle?) {
        progressDialog = ProgressDialog.getInstance(requireContext())
        binding.recyclerView.setLinearLayoutManager(requireContext())
        binding.recyclerView.adapter = githubRepositoryAdapter

        githubRepoViewModel.getRepositories("android", "stars")
    }

    override fun observeData() {
        CoroutineScope(Dispatchers.Main).launch {
            githubRepoViewModel.repositoryList.collectLatest {
                if (it.isLoading) {
                    progressDialog.show()
                } else progressDialog.dismiss()

                if (it.error.isNotBlank()) it.error.longSnackBar()

                if (it.repositoryList.isNotEmpty()) {
                    githubRepositoryAdapter.addItems(it.repositoryList)
                }
            }
        }
    }

    override fun onClicked(item: Repository, position: Int) {
        session.setObject(item)
        navigate(R.id.home_to_repo_details)
    }
}