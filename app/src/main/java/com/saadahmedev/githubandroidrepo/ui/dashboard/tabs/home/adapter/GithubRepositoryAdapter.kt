package com.saadahmedev.githubandroidrepo.ui.dashboard.tabs.home.adapter

import com.saadahmedev.githubandroidrepo.R
import com.saadahmedev.githubandroidrepo.base.BaseRecyclerAdapter
import com.saadahmedev.githubandroidrepo.databinding.ItemLayoutRepositoryBinding
import com.saadahmedev.githubandroidrepo.domain.model.Repository
import com.saadahmedev.githubandroidrepo.helper.onClicked
import com.squareup.picasso.Picasso

class GithubRepositoryAdapter(private val listener: OnItemClickListener) : BaseRecyclerAdapter<Repository, ItemLayoutRepositoryBinding>() {

    override val layoutRes: Int
        get() = R.layout.item_layout_repository

    override fun onBind(binding: ItemLayoutRepositoryBinding, item: Repository, position: Int) {
        Picasso.get().load(item.avatarUrl).into(binding.ivAvatar)
        binding.item = item

        binding.root.onClicked {
            listener.onClicked(item, position)
        }
    }
}