package com.saadahmedev.githubandroidrepo.helper

fun currentTime() = System.currentTimeMillis()

fun currentTimeToString() = currentTime().toString()