package com.saadahmedev.githubandroidrepo.domain.useCase

import com.saadahmedev.githubandroidrepo.domain.model.Repository
import com.saadahmedev.githubandroidrepo.domain.repository.GithubRepository
import com.saadahmedev.githubandroidrepo.util.ResponseState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class GithubRepositoryListUseCase @Inject constructor(private val githubRepository: GithubRepository) {

    operator fun invoke(repoType: String, sortBy: String): Flow<ResponseState<List<Repository>>> = flow {
        try {
            emit(ResponseState.Loading())

            val repositoryList = githubRepository.getRepositories(repoType, sortBy).items.map {
                it.toRepository()
            }

            emit(ResponseState.Success(repositoryList))
        }
        catch (e: Exception) {
            emit(ResponseState.Error(e.localizedMessage?: "Unexpected error occurred"))
        }
    }
}