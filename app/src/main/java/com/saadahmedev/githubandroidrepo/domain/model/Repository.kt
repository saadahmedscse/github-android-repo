package com.saadahmedev.githubandroidrepo.domain.model

data class Repository(
    val repoName: String? = null,
    val ownerName: String? = null,
    val starsCount: Int? = null,
    val avatarUrl: String? = null,
    val repoUrl: String? = null,
    val forkCount: Int? = null,
    val issueCount: Int? = null,
    val watcherCount: Int? = null,
    val topics: List<String> = arrayListOf(),
    val description: String? = null,
    val defaultBranch: String? = null,
    val language: String? = null,
    val licenseName: String? = null,
    val updatedAt: String? = null
)
