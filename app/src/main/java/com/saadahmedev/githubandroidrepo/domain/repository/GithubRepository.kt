package com.saadahmedev.githubandroidrepo.domain.repository

import com.saadahmedev.githubandroidrepo.data.dto.GithubRepoResponse

interface GithubRepository {
    suspend fun getRepositories(repoType: String, sortBy: String): GithubRepoResponse
}