package com.saadahmedev.githubandroidrepo.data.source

import com.saadahmedev.githubandroidrepo.BuildConfig
import com.saadahmedev.githubandroidrepo.data.dto.GithubRepoResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GithubApi {

    @GET("search/repositories")
    @Headers("Accept: application/json", "Application-Package: ${BuildConfig.APPLICATION_ID}")
    suspend fun getRepositories(@Query("q") repoType: String, @Query("sort") sortBy: String): GithubRepoResponse
}