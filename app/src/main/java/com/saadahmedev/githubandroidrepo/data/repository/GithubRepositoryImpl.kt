package com.saadahmedev.githubandroidrepo.data.repository

import com.saadahmedev.githubandroidrepo.data.dto.GithubRepoResponse
import com.saadahmedev.githubandroidrepo.data.source.GithubApi
import com.saadahmedev.githubandroidrepo.domain.repository.GithubRepository
import javax.inject.Inject

class GithubRepositoryImpl @Inject constructor(private val githubApi: GithubApi) : GithubRepository {

    override suspend fun getRepositories(repoType: String, sortBy: String): GithubRepoResponse {
        return githubApi.getRepositories(repoType, sortBy);
    }
}