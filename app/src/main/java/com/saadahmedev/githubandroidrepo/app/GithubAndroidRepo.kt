package com.saadahmedev.githubandroidrepo.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GithubAndroidRepo : Application() {
}