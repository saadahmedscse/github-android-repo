package com.saadahmedev.githubandroidrepo.util

class AppConstants {
    object AppInfo {
        const val APP_NAME = "Github Android Repo"
    }

    object Api {
        const val BASE_URL = "https://api.github.com/"
    }
}