package com.saadahmedev.githubandroidrepo.di

import com.saadahmedev.githubandroidrepo.data.repository.GithubRepositoryImpl
import com.saadahmedev.githubandroidrepo.data.source.GithubApi
import com.saadahmedev.githubandroidrepo.domain.repository.GithubRepository
import com.saadahmedev.githubandroidrepo.util.AppConstants.Api.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ProjectModule {

    @Provides
    @Singleton
    fun provideGithubApi(): GithubApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GithubApi::class.java)
    }

    @Provides
    @Singleton
    fun provideGithubRepository(githubApi: GithubApi): GithubRepository {
        return GithubRepositoryImpl(githubApi)
    }
}